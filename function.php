<?php
/**
 * Simple functions
 *
 * @category  Cron
 * @package
 * @author    Deli-Szabó Sándor <sandor@deliszabo.com>
 * @copyright 2014 Deli-Szabó Sándor. (http://deliszabo.com)
 * @license   http://deliszabo.com/free/license
 * @link      http://deliszabo.com
 *
 */
?>
<?php

/**
 * error log function for cron.php
 * @param      $message
 * @param bool $kill
 */
function error($message, $kill = false)
{

        echo $message."<br>\n";

        $message = '[' . date('Y-m-d H:i:s') . '] ' . $message ."\n";

        file_put_contents('log/system.cron.log', $message, FILE_APPEND);

        if ($kill){
            die('exit');
        }

}

/**
 * current date/time
 * @return string
 */
function now()
{
    return date('Y-m-d H:i:s');
}

/**
 * current date/time
 * @return integer
 */
function nowTime()
{
    return timetostr(now());
}

/**
 * parse cron style time
 * @param $time
 * @param $crontab
 * @return mixed
 */
function parse_crontab($time, $crontab)
{
    $time=explode(' ', date('i G j n w', strtotime($time)));
    $crontab=explode(' ', $crontab);
    foreach ($crontab as $k=>&$v){
        $v=explode(',', $v);
        foreach ($v as &$v1){
            $v1=preg_replace(
                    array('/^\*$/', '/^\d+$/', '/^(\d+)\-(\d+)$/', '/^\*\/(\d+)$/'),
                    array('true', $time[$k].'===\0', '(\1<='.$time[$k].' and '.$time[$k].'<=\2)', $time[$k].'%\1===0'),
                    $v1
                );
        }
        $v='('.implode(' or ', $v).')';
    }
    $crontab=implode(' and ', $crontab);
    return eval('return '.$crontab.';');
}

