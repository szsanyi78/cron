<?php 
/**
 * Demo cron class
 * 
 * @category  Cron
 * @package
 * @author    Deli-Szabó Sándor <sandor@deliszabo.com>
 * @copyright 2014 Deli-Szabó Sándor. (http://deliszabo.com)
 * @license   http://deliszabo.com/free/license
 * @link      http://deliszabo.com
 *
 */
?>
<?php

class demo Extends Cron
{

    public $config = '*/5 * * * *';


    /**
     * run script this cron module
     * database: $this->db - pdo database object
     */
    public function run()
    {
        $this->log('running demo cron script!', true);
    }


}


?>
