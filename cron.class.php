<?php 
/**
 * Cron class
 * 
 * @category  Cron
 * @package
 * @author    Deli-Szabó Sándor <sandor@deliszabo.com>
 * @copyright 2014 Deli-Szabó Sándor. (http://deliszabo.com)
 * @license   http://deliszabo.com/free/license
 * @link      http://deliszabo.com
 *
 */
?>
<?php

class Cron
{
    public $db = null;
    public $log = null;

    /**
     * construct
     * @param $db: pdo database object
     */
    function __construct($db)
    {
        $this->db = $db;
    }


    /**
     * log to file and display
     * @param      $message
     * @param bool $display
     */
    public function log($message, $display = false)
    {
        if ($this->log==null){
            $this->log = 'log/' . get_class($this) . '.log';
        }

        if (is_array($message)){
            $message = json_encode($message);
        }
        $message = '[' . date('Y-m-d H:i:s') . '] ' . $message ."\n";

        file_put_contents($this->log, $message, FILE_APPEND);

        if ($display){
            echo $message;
        }

    }

    /**
     * set pdo database object
     * @param $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }


}


?>
