<?php 
/**
 * Cron start script
 * 
 * @category  Cron
 * @package
 * @author    Deli-Szabó Sándor <sandor@deliszabo.com>
 * @copyright 2014 Deli-Szabó Sándor. (http://deliszabo.com)
 * @license   http://deliszabo.com/free/license
 * @link      http://deliszabo.com
 *
 */
?>
<?php
require_once "config.php";
require_once "function.php";
require_once "cron.class.php";

if (!file_exists('log')){
    @mkdir('log', 0777, true);
}

if (!file_exists('scripts')){
    error ('Not found cron script directory!  (CURRENT DIR/scripts)', true);
}


if (DB_NAME!=''){
    $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
    $db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS, $options);
} else {
    $db = null;
}



foreach(glob('scripts/*.php') as $file){

    $stripname = str_replace(array('scripts/', '.php'), '', $file);
    $error = false;
    try{
        require_once($file);
        $class = $stripname;
        $cron = new $class($db);
    } catch (Exception $e) {
        error('error load '.$file.' cron class  (' . $e->getMessage() . ')', false);
        $error = true;
    }

    if (!$error){

        $crontime = $cron->config;

        if (parse_crontab(now(), $cron->config)){
            try{
                $cron->setDb($db);
                $cron->run();
            } catch (Exception $e) {
                error('error run '.$class.' cron class  (' . $e->getMessage() . ')', false);
                $error = true;
            }
        }
    }
}


?>
